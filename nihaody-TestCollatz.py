#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # added tests

    def test_read1(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read2(self):
        s = "1 3 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 3)

    def test_read3(self):
        s = "10000 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10000)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # added tests

    def test_eval_5(self):
        v = collatz_eval(46, 76)
        self.assertEqual(v, 116)

    def test_eval_6(self):
        v = collatz_eval(1000, 2)
        self.assertEqual(v, 179)

    def test_eval_7(self):
        v = collatz_eval(20, 9)
        self.assertEqual(v, 21)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # added tests

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 46, 1000, 179)
        self.assertEqual(w.getvalue(), "46 1000 179\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 250, 1, 128)
        self.assertEqual(w.getvalue(), "250 1 128\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 20, 19, 21)
        self.assertEqual(w.getvalue(), "20 19 21\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # added tests

    def test_solve1(self):
        r = StringIO("23 27\n35 36\n10000 345\n100 245\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "23 27 112\n35 36 22\n10000 345 262\n100 245 128\n")

    def test_solve2(self):
        r = StringIO("3 2\n100 956\n690 4\n22 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3 2 8\n100 956 179\n690 4 145\n22 10 21\n")

    def test_solve3(self):
        r = StringIO("1 54\n900 23\n45 100\n76 46\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 54 113\n900 23 179\n45 100 119\n76 46 116\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
